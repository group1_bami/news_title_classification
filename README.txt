Text Classification Berita Online Bahasa Indonesia

Proyek ini bertujuan untuk mengklasifikasikan berita online ke dalam beberapa kategori berdasarkan judul berita.

Models:
Naive Bayes Classifier
SVM Classifier

Requirements
Phython 3.x
Pandas
Numpy
Pickle
sklearn

Data format:
Training Data disimpan dalam bentuk file csv. Baris pertama adalah Judul, Label, dan Kategori

Run:
Proyek dapat dirun dengan menggunakan cmd di directory file tersebut disimpan.

